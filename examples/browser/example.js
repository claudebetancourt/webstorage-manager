var store = webStorageManager.default;

store.configure({
  namespace: "browser",
});

console.log("store", store);

var sampleUser = {
  name: "Joseph Blow",
  email: ["jb@gmail.com", "joeb@yahoo.com"],
  zipCode: 10017,
  lastUpdated: new Date().toISOString(),
};

store.clear();
store.add("foo", "bar");
store.add("user", sampleUser);
store.add("baz", "qux");

console.group("Browser Tests");
console.log("config:", store.config);
console.log("size:", store.size());
console.log("list keys:", store.keys());
console.log("get foo:", store.getValue("foo"));
console.log("get user:", store.getValue("user"));
console.log("remove baz:", store.remove("baz"));
console.log("list keys:", store.keys());
console.log("addIfNotPresent foo", store.addIfNotPresent("foo", "baz"));
console.log("get all values", store.getData());
console.groupEnd();
