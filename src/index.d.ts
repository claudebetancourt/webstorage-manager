export default instance;
declare const instance: WebStorageManager;
declare class WebStorageManager {
    constructor(options?: {});
    /**
     * Configures the store instance
     *
     * @param {object} options Configuration options
     * @returns {Function} An instance of WebStorageManager
     */
    configure(options: object): Function;
    config: {
        storage: any;
        namespace: any;
        created: Date;
    };
    storage: any;
    namespace: string;
    _getNamespacedKey(key: any): string;
    _getOriginalKey(namespacedKey: any): any;
    _getKeys(namespaced?: boolean): any[];
    _removeKeys(): void;
    /**
     * Applies the given function to each member
     *
     * @param {Function} callback Callback function to execute
     * @param {boolean} namespaced Whether the key should include the namespace. Defaults to false
     */
    iterator(callback: Function, namespaced?: boolean): void;
    /**
     * Inserts or updates key with the provided value
     *
     * @param {string} key The key to be added
     * @param {object | string | number | null} value The value to be added
     */
    add(key: string, value: object | string | number | null): void;
    /**
     * Inserts key with the provided value when key does not already exist
     *
     * @param {string} key The key to be added
     * @param {object | string | number | null} value The value to be added
     * @returns {boolean} Whether the value was inserted
     */
    addIfNotPresent(key: string, value: object | string | number | null): boolean;
    /**
     * Returns key for given index, or null
     *
     * @param {number} index The index to look up
     * @returns {string|null} The value associated with the requested index
     */
    getKey(index: number): string | null;
    /**
     * Returns the value for the given key, or null when not found
     *
     * @param {string} key The key to look up
     * @returns {object} The value associated with the requested key
     */
    getValue(key: string): object;
    /**
     * Returns all data stored in the current namespace as a list of key/value pairs
     *
     * @returns {Array} The list of keys and values for the current namespace
     */
    getData(): any[];
    /**
     * Removes value for given key stored in the current namespace
     *
     * @param {string} key The key to remove
     */
    remove(key: string): void;
    /**
     * Removes all keys and values stored in the current namespace
     */
    clear(): void;
    /**
     * Returns list of keys stored in the current namespace
     *
     * @returns {Array} List of keys
     */
    keys(): any[];
    /**
     * Returns the total number of objects stored in the current namespace
     *
     * @returns {number} Total number of keys stored in the current namespace
     */
    size(): number;
}
