export function isValidString(value: string): boolean;
export function isValidStorage(type: string): boolean;
export function serialize(value: object): string;
export function deserialize(value: string): object;
