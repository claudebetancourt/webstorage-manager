/**
 * Iterates over the provided store and calls the provided function
 * with each namespaced key and value pair
 *
 * @param {Function} store An instance of WebStorageManager
 * @param {Function} cb Callback function to execute
 */
export const iterateStorage = (store, cb) => {
  const deserialize = require("./utilities").deserialize;
  const { storage, namespace } = store;
  Object.keys(storage)
    .filter((key) => key.startsWith(namespace))
    .forEach((key) => cb(key, deserialize(storage[key])));
};
