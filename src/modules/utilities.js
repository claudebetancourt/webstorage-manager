/**
 * Tests whether a value is a string
 *
 * @param {string} value The value to test
 * @returns {boolean} boolean
 */
export const isValidString = (value) =>
  typeof value === "string" && value.trim() !== "";

/**
 * Tests whether the passed web storage is valid
 *
 * @param {string} type web storage type, either localStorage or sessionStorage
 * @returns {boolean} boolean
 */
export const isValidStorage = (type) => {
  const test = {
    key: "webStorageManager.test",
    value: "This is a a test of webStorageManager",
  };

  try {
    type.setItem(test.key, test.value);
    type.getItem(test.key);
    type.removeItem(test.key);
    return true;
  } catch (err) {
    return false;
  }
};

/**
 * Converts a value to JSON for storage
 *
 * @param {object} value The value to serialize
 * @returns {string} JSON string
 */
export const serialize = (value) => {
  if (value) {
    try {
      return JSON.stringify(value);
    } catch (err) {
      //
    }
  }
  return value;
};

/**
 * Deserializes a JSON string
 *
 * @param {string} value The JSON string to deserialize
 * @returns {object} JSON object
 */
export const deserialize = (value) => {
  if (value) {
    try {
      return JSON.parse(value);
    } catch (err) {
      //
    }
  }
  return value;
};
