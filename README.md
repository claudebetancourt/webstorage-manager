# Web Storage Manager

A convenient utility for working with web storage. It provides a simple and intuitive API to manage [data storage in modern browsers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API) that can be easily configured to use persistent [localStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) (default) or [sessionStorage](https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage).

![License](https://badgen.net/npm/license/@cbetancourt/webstorage-manager) ![Latest Version](https://badgen.net/npm/v/@cbetancourt/webstorage-manager) ![Minified Size](https://badgen.net/bundlephobia/min/@cbetancourt/webstorage-manager) ![Minified and Zipped Size](https://badgen.net/bundlephobia/minzip/@cbetancourt/webstorage-manager) [![codecov](https://codecov.io/gl/claudebetancourt/webstorage-manager/branch/master/graph/badge.svg)](https://codecov.io/gl/claudebetancourt/webstorage-manager)

In order to avoid overwriting others' data, the utility automatically namespaces your keys. The default namespace, `store`, can be overridden via configuration options.

This utility is exposed as an ESM module as well as older module definitions like CommonJS and AMD. It can also be used as a global variable.

## API

```js
// Configures the store instance.
configure({
  storage: window.localStorage,
  namespace: "store"
}) : store

// Applies the provided function to each member of the store
iterator(Function callback, Boolean namespaced = false) : void

// Inserts or updates key with the provided value
add(String key, [Object|String|Number|null] value) : void

// Inserts key with the provided value when key does not already exist
addIfNotPresent(String key, [Object|String|Number|null] value) : Boolean

// Returns key for provided index, or null
getKey(Number index) : String

// Returns the value for the provided key, or null when not found
getValue(String key) : [Object|String|Number|null]

// Returns all data stored in the current namespace as a list of key/value pairs
getData() : Array

// Removes value for provided key stored in the current namespace
remove(String key) : void

// Removes all keys and values stored in the current namespace
clear() : void

// Returns list of keys stored in the current namespace
keys() : Array

// Returns the total number of objects stored in the current namespace
size() : Number
```

---

## Installation

Install this utility using your favorite package manager:

### npm

```bash
npm install @cbetancourt/webstorage-manager
```

### yarn

```bash
yarn add @cbetancourt/webstorage-manager
```

### git

```bash
git clone https://gitlab.com/claudebetancourt/webstorage-manager.git
```

## Usage

After installing the utility, you can import it into your application and begin using it. All your data will be saved under the default namespace, `store`.

```js
import webStorageManager from "@cbetancourt/webstorage-manager";

// add data to your store
webStorageManager.add("foo", "bar");

webStorageManager.add("baz", "qux");

webStorageManager.add("user", {
  name: "Joseph Blow",
  email: ["jb@gmail.com", "joeb@yahoo.com"],
  zipCode: 10017
});

// get keys
const keys = webStorageManager.keys(); // ["foo","baz","user"]
```

## Configuration

### Options

You can specify the following options to configure your store:

- `namespace` must be a non-null string.
  - **Defaults to `store`**.
- `storage` must implement the [Web Storage API](https://developer.mozilla.org/en-US/docs/Web/API/Storage). Modern browsers provide two options, `Window.localStorage` and `Window.sessionStorage`.
  - **Defaults to `Window.localStorage`**

Calling the `configure()` method will validate the provided `namespace` and `storage` options. Invalid options will result in the only exceptions thrown by the utility.

### Custom Namespace

The following example configures the store to save your data under the namespace `myApp`:

```js
import webStorageManager from "@cbetancourt/webstorage-manager";

const store = webStorageManager.configure({
  namespace: "myApp"
});

// add data to your store
store.add("foo", "bar");

store.add("baz", "qux");

store.add("user", {
  name: "Joseph Blow",
  email: ["jb@gmail.com", "joeb@yahoo.com"],
  zipCode: 10017
});

// get keys
const keys = store.keys(); // ["foo","baz","user"]
```

### Use Session Storage

The following example configures the store to save your data in sessionStorage under the namespace `myApp`:

```js
import webStorageManager from "@cbetancourt/webstorage-manager";

const store = webStorageManager.configure({
  storage: sessionStorage,
  namespace: "myApp"
});

// add data to your store
store.add("foo", "bar");

store.add("baz", "qux");

store.add("user", {
  name: "Joseph Blow",
  email: ["jb@gmail.com", "joeb@yahoo.com"],
  zipCode: 10017
});

// get keys
const keys = store.keys(); // ["foo","baz","user"]
```

## Build for development

```bash
npm install && npm start
```

## Build for production

```bash
npm install && npm run build
```

## Test

```bash
npm install && npm test
```

## Changelog

Read about features and bug fixes in the [CHANGELOG](https://gitlab.com/claudebetancourt/webstorage-manager/blob/master/CHANGELOG.md).
