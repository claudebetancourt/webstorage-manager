# Changelog

## 2.0.2

Update documentation

## 2.0.1

Convert to ESM

## 1.3.0

Upgrade development dependencies and clean up tests.

## 1.2.0

- Switched from Yarn to NPM.
- Updated build system dependencies to address 63 low severity vulnerabilities.
- Removed superflous source-map.

## 1.1.2

- Added `Object.assign()` utility.
- Added tests.

## 1.1.1

- Added tests.
- Added code coverage scripts.

## 1.1.0

- API includes two new public methods:
  - `iterator() : void`
  - `getData() : Array`
- Adds tests for new methods.
- Improved documentation.

## 1.0.0

Initial release.
