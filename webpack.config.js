const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const { mode } = require("yargs").argv;
const license = require('./license');
const pkg = require("./package.json");

const libraryName = "webStorageManager";
const devtool = "eval";

const entry = `${__dirname}/src/index.js`;

const output = {
  library: {
    name: libraryName,
    type: "umd",
    umdNamedDefine: true,
  },
  path: `${__dirname}/dist`,
  filename: `index.js`,
};

const banner = `${pkg.name}
${pkg.description}

@version ${pkg.version}
@homepage ${pkg.homepage}
@issues ${pkg.bugs.url}

${license(pkg.author)}`;

const plugins = [
  new webpack.BannerPlugin(banner),
  new CopyPlugin({
    patterns: [{ from: "./src/index.d.ts", to: "./" }],
  }),
  new CleanWebpackPlugin(),
];

const config = {
  mode,
  entry,
  output,
  devtool,
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: "eslint-loader",
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
    ],
  },
  plugins,
};

// production overrides
if (mode === "production") {
  delete config.devtool;
}

module.exports = config;
