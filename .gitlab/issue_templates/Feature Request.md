## Problem to be Solved
Describe the existing problem or limitation

## Target Audience
For whom are we doing this? Include either a persona from https://design.gitlab.com/getting-started/personas or define a specific company role. e.a. "Release Manager" or "Security Analyst"

## Further Details
Include use cases, benefits, and/or goals (contributes to our vision?)

## Proposal
How are we going to solve the problem?

## What does success look like, and how can we measure that?
If no way to measure success, link to an issue that will implement a way to measure this

## Links / References

/label ~"feature request"
