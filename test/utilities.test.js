/**
 * @jest-environment jsdom
 */

import { UTILITY_OBJECT_ASSIGN_TYPE_ERROR } from "../src/modules/constants";

describe("WebStorageManager: Object.assign() with a null target", () => {
  it(`Throws Type Error: ${UTILITY_OBJECT_ASSIGN_TYPE_ERROR}`, () => {
    expect(() => {
      Object.assign(null, { foo: "bar" });
    }).toThrowError(UTILITY_OBJECT_ASSIGN_TYPE_ERROR);
  });
});

describe("WebStorageManager: Object.assign() with a deep nested object", () => {
  const deep = Object.assign(
    { foo: "bar", baz: null },
    {
      foo: {
        x: 1,
        y: 2,
        z: {
          a: "A"
        }
      }
    },
    null
  );
  it("Has foo.z.a", () => {
    expect(deep.foo.z.a).toBeDefined();
  });
  it("foo.z.a is 'A'", () => {
    expect(deep.foo.z.a).toEqual("A");
  });
});
