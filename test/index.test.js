/**
 * @jest-environment jsdom
 */

import { INVALID_STORAGE, INVALID_NAMESPACE } from "../src/modules/constants";
import store from "../src/index";
// import store from "../dist/index";

describe("WebStorageManager: Default Config", () => {
  expect(store.constructor.name).toBeDefined();
});

it("Has config object", () => {
  expect(store.config).toBeInstanceOf(Object);
});

it("Has 'storage' config option", () => {
  expect(store.config.storage).toBeDefined();
});

it("Has 'namespace' config option", () => {
  expect(store.config.namespace).toBeDefined();
});

it("Has 'namespace' config option set to 'store'", () => {
  expect(store.config.namespace).toEqual("store");
});

it("Has 'namespace' property", () => {
  expect(store.namespace).toBeDefined();
});

it(`Has 'namespace' property set to 'store.'`, () => {
  expect(store.namespace).toEqual("store.");
});

it("Store is empty", () => {
  expect(store.size()).toEqual(0);
});

it("An empty list is returned", () => {
  const data = store.getData();
  expect(data).toEqual([]);
});

it("Non-existent key should return null", () => {
  const result = store.getValue("foo");
  expect(result).toBeNull();
});

it("Non-existent index should return null", () => {
  const result = store.getKey(0);
  expect(result).toBeNull();
});

describe("WebStorageManager: Invalid storage type", () => {
  const config = {
    storage: null
  };

  it("Throws error on config()", () => {
    expect(() => {
      store.configure(config);
    }).toThrowError(INVALID_STORAGE);
  });
});

describe("WebStorageManager: Invalid namespace", () => {
  const config = {
    namespace: null
  };

  it("Throws error on config()", () => {
    expect(() => {
      store.configure(config);
    }).toThrowError(INVALID_NAMESPACE);
  });
});

describe("WebStorageManager: Use Custom Namespace", () => {
  const config = {
    namespace: "myCustomNamespace"
  };

  beforeAll(() => {
    store.configure(config);
  });

  it("Store instance exists", () => {
    expect(store.constructor.name).toBeDefined()
  });

  it("Has config object", () => {
    expect(store.config).toBeInstanceOf(Object);
  });

  it("Has 'storage' config option", () => {
    expect(store.config.storage).toBeDefined();
  });

  it("Has 'namespace' config option", () => {
    expect(store.config.namespace).toBeDefined();
  });

  it(`Has 'namespace' config option set to ${config.namespace}`, () => {
    expect(store.config.namespace).toEqual(config.namespace);
  });

  it("Has 'namespace' property", () => {
    expect(store.namespace).toBeDefined();
  });

  it(`Has 'namespace' property set to '${config.namespace}.'`, () => {
    expect(store.namespace).toEqual(`${config.namespace}.`);
  });

  it("Store is empty", () => {
    expect(store.size()).toEqual(0);
  });
});

describe("WebStorageManager: CRUD for Simple Values", () => {
  const config = {
    namespace: "test1"
  };

  const sample1 = {
    key: "foo",
    value: "bar"
  };

  const sample2 = {
    key: "foo",
    value: "baz"
  };

  const sample3 = {
    key: "baz",
    value: "qux"
  };

  const sample4 = {
    key: "age",
    value: 33
  };

  const sample5 = {
    key: "user",
    value: null
  };

  beforeAll(() => {
    store.configure(config);
  });

  it("Store instance exists", () => {
    expect(store.constructor.name).toBeDefined();
  });

  it(`Has 'namespace' property set to '${config.namespace}.'`, () => {
    expect(store.namespace).toEqual(`${config.namespace}.`);
  });

  it("Store is empty", () => {
    expect(store.size()).toEqual(0);
  });

  it(`Inserts key '${sample1.key}' with value of '${sample1.value}'`, () => {
    store.add(sample1.key, sample1.value);
    expect(store.size()).toEqual(1);
  });

  it(`Value of key '${sample1.key}' is '${sample1.value}'`, () => {
    expect(store.getValue(sample1.key)).toEqual(sample1.value);
  });

  it(`Updates key '${sample2.key}' with value of '${sample2.value}'`, () => {
    store.add(sample2.key, sample2.value);
    expect(store.size()).toEqual(1);
  });

  it(`Value of key '${sample2.key}' is now '${sample2.value}'`, () => {
    expect(store.getValue(sample2.key)).toEqual(sample2.value);
  });

  it(`Does not allow overwriting value of '${sample2.key}'`, () => {
    const result = store.addIfNotPresent(sample2.key, sample2.value);
    expect(result).toBeFalsy();
  });

  it(`Value of key '${sample2.key}' is still '${sample2.value}'`, () => {
    expect(store.getValue(sample2.key)).toEqual(sample2.value);
  });

  it(`Retrieves key, '${sample2.key}', for first item in store`, () => {
    const key = store.getKey(0);
    expect(key).toEqual(sample2.key);
  });

  it(`Inserts key '${sample3.key}' with value of '${sample3.value}'`, () => {
    store.addIfNotPresent(sample3.key, sample3.value);
    expect(store.size()).toEqual(2);
  });

  it(`List of keys contain '${sample2.key}' and '${sample3.key}'`, () => {
    const keys = store.keys();
    expect(keys).toContain(sample2.key);
    expect(keys).toContain(sample3.key);
  });

  it(`Removes '${sample2.key}'`, () => {
    store.remove(sample2.key);
    expect(store.size()).toEqual(1);
  });

  it(`Inserts key '${sample4.key}' with value of '${sample4.value}'`, () => {
    store.addIfNotPresent(sample4.key, sample4.value);
    expect(store.size()).toEqual(2);
  });

  it(`Value of key '${sample4.key}' should be ${sample4.value}`, () => {
    const result = store.getValue(sample4.key);
    expect(result).toEqual(sample4.value);
  });

  it(`Inserts key '${sample5.key}' with value of '${sample5.value}'`, () => {
    store.addIfNotPresent(sample5.key, sample5.value);
    expect(store.size()).toEqual(3);
  });

  it(`Value of key '${sample5.key}' should be ${sample5.value}`, () => {
    const result = store.getValue(sample5.key);
    expect(result).toEqual(sample5.value);
  });

  it(`Store contains the keys '${sample3.key}', '${sample4.key}' and '${sample5.key
    }' and their values`, () => {
      const data = store.getData();
      expect(data[0].key).toEqual(sample3.key);
      expect(data[0].value).toEqual(sample3.value);
      expect(data[1].key).toEqual(sample4.key);
      expect(data[1].value).toEqual(sample4.value);
      expect(data[2].key).toEqual(sample5.key);
      expect(data[2].value).toEqual(sample5.value);
    });

  it("Clears store", () => {
    store.clear();
    expect(store.size()).toEqual(0);
    expect(store.keys()).toEqual([]);
  });
});

describe("WebStorageManager: CRUD for Complex Values", () => {
  const config = {
    namespace: "test2",
    storage: sessionStorage
  };

  const sample1 = {
    key: "location",
    value: {
      pathname: "/report",
      search: "?start=101&size=100",
      hash: ""
    }
  };

  const sample2 = {
    key: "user",
    value: {
      name: "Joseph Blow",
      email: ["jb@gmail.com", "joeb@yahoo.com"],
      zipCode: 10017
    }
  };

  beforeAll(() => {
    store.configure(config);
  });

  it("Store instance exists", () => {
    expect(store.constructor.name).toBeDefined();
  });

  it(`Has 'namespace' property set to '${config.namespace}.'`, () => {
    expect(store.namespace).toEqual(`${config.namespace}.`);
  });

  it("Store is empty", () => {
    expect(store.size()).toEqual(0);
  });

  it(`Inserts key '${sample1.key}' with value of '${sample1.value}'`, () => {
    store.add(sample1.key, sample1.value);
    expect(store.size()).toEqual(1);
  });

  it(`Value of key '${sample1.key}' is '${sample1.value}'`, () => {
    expect(store.getValue(sample1.key)).toEqual(sample1.value);
  });

  it(`Inserts key '${sample2.key}' with value of '${sample2.value}'`, () => {
    store.add(sample2.key, sample2.value);
    expect(store.size()).toEqual(2);
  });

  it(`Value of key '${sample2.key}' is '${sample2.value}'`, () => {
    expect(store.getValue(sample2.key)).toEqual(sample2.value);
  });

  it(`Store contains the keys '${sample1.key}' and '${sample2.key
    }', and their values`, () => {
      const data = store.getData();
      expect(data[0].key).toEqual(sample1.key);
      expect(data[0].value).toEqual(sample1.value);
      expect(data[1].key).toEqual(sample2.key);
      expect(data[1].value).toEqual(sample2.value);
    });

  it("Clears store", () => {
    store.clear();
    expect(store.size()).toEqual(0);
    expect(store.keys()).toEqual([]);
  });
});
